#include <stdio.h>
#include <stdlib.h>

int main(void) {
    int menor, mayor, filasilla, columnasilla,sumatotal;

    int fil, colum;

    printf("Introduzca un numero entero para las filas: ");
    scanf("%d", &fil);
    printf("Introduzca un numero entero pra las columnas: ");
    scanf("%d", &colum);

    int matriz[fil][colum];

    //llenar la matriz 
    printf("\n\n Ingrese valores a la MATRIZ \n");

    for (int i = 0; i < fil; i++) {
        for (int j = 0; j < colum; j++) {
            printf("\nintroduzca un valor para la matriz en el punto [%d][%d]: ", i, j);
            scanf("%d", &matriz[i][j]);
        }
        printf("\n\n");
    }

    //mostrar la matriz
    printf("\nLa Matriz ingresada es:\n");
    for (int i = 0; i < fil; i++) {
        for (int j = 0; j < colum; j++) {

            printf("%d\t", matriz[i][j]);

        }
        printf("\n");
    }

//saber la sumatoria total de la matriz
    for (int i = 0; i < fil; i++) {
        for (int j = 0; j < colum; j++) {
            sumatotal=sumatotal+matriz[i][j];
            
        }
    }


    //buscando el punto de silla

    for (int j = 0; j < colum; j++) {

        mayor = -sumatotal, menor = sumatotal;
        for (int i = 0; i < fil; i++) {
            if (matriz[i][j] > mayor) {
                mayor = matriz[i][j];
                filasilla = i;
            }
        }
        for (int k = 0; k < colum; k++) {
            if (matriz[filasilla][k] < menor) {
                menor = matriz[filasilla][k];
                columnasilla = k;
            }
        }
        if (mayor == menor)
            printf("\nPunto de silla esta en el punto: [%d][%d]y es= %d\n", filasilla, columnasilla, matriz[filasilla][columnasilla]);

    }
} 
